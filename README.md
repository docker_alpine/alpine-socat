# alpine-socat
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-socat)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-socat)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-socat/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-socat/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [socat](http://www.dest-unreach.org/socat/)
    - Multipurpose relay



----------------------------------------
#### Run

```sh
docker run -d \
           -e TCP_PORT={TCP_PORT} \
           -e SOCKET_PATH={SOCKET_PATH} \
           -e SOCKET_ADDR={SOCKET_ADDR} \
           -p {TCP_PORT}:{TCP_PORT}/tcp \
           forumi0721/alpine-socat:[ARCH_TAG]
```



----------------------------------------
#### Usage

* Nothing to do



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| {TCP_PORT}/tcp     | TCP port                                         |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| TCP_PORT           | TCP port                                         |
| SOCKET_PATH        | Socket path                                      |
| SOCKET_ADDR        | Socket address                                   |

